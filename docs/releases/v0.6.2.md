### 0.6.2 Changelog

- Add configuration for url validation on redirection
- Improve url helpers by pulling in default app configuration
- Make redirect url processing and validation more flexible
