# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Dsl do
  subject { Class.new { extend Gitlab::Experiment::Dsl } }

  let(:klass) do
    Class.new do
      def self.helper_method(name)
        @helpers ||= []
        @helpers << name
      end
    end
  end

  it "can include itself into things" do
    described_class.include_in(klass)

    expect(klass.instance_methods).to include(:experiment)
    expect(klass.instance_variable_get(:@helpers)).to be_nil

    described_class.include_in(klass, with_helper: true)
    expect(klass.instance_variable_get(:@helpers)).to eq([:experiment])
  end

  it "merges the request into the context if there's a request available" do
    request = double(headers: {}, cookie_jar: double(signed: { 'gitlab_experiment_example_id' => '_cookie_' }))

    allow(subject).to receive(:respond_to?).with(:request).and_return(true)
    allow(subject).to receive(:request).and_return(request)

    expect(subject.experiment(:example, foo: 'bar').context.value).to eq(
      actor: '_cookie_',
      foo: 'bar'
    )
  end
end
