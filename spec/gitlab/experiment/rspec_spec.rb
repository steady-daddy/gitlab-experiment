# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::RSpecHelpers, :experiment do
  describe ".stub_experiments" do
    it "allows stubbing of an experiment" do
      stub_experiments({ stub: :foo }, 2)

      expect(experiment(:stub)).to be_enabled
      expect(StubExperiment.new.assigned.name).to eq('foo')
    end

    it "handles blocks appropriately" do
      stub_experiments(stub: :foo)

      experiment(:stub) do |e|
        e.variant(:foo) {}
        expect(e).to be_enabled
        expect(e.assigned.name).to eq('foo')
      end
    end

    it "stubs experiments that may not have a class definition" do
      stub_experiments(foo: :bar)

      experiment(:foo) do |e|
        e.variant(:bar) {}
        expect(e).to be_enabled
        expect(e.assigned.name).to eq('bar')
      end
    end

    it "stubs only the experiments we specify" do
      stub_experiments(hippy: :free_love)

      expect do
        experiment(:normie) do |e|
          e.control {}
          e.candidate {}
        end
      end.not_to raise_error

      expect(experiment(:normie).assigned.name).to eq("control")
      expect(experiment(:hippy).assigned.name).to eq("free_love")
    end
  end

  describe ".wrapped_experiment" do
    it "allows wrapping :new on an experiment type" do
      wrapped_experiment(:stub) { |e| e.assigned(:foo) }

      expect(experiment(:stub).assigned.name).to eq('foo')
    end

    it "allows wrapping from an instance" do
      wrapped_experiment(experiment(:stub)) { |e| e.assigned(:foo) }

      expect(experiment(:stub).assigned.name).to eq('foo')
    end

    it "calls the block provided to new" do
      wrapped_experiment(:stub, 2) { |e| e.assigned(:foo) }

      instance = experiment(:stub) do |e|
        e.assigned(:bar)
        e.variant(:bar) {}
      end

      expect(instance.assigned.name).to eq('bar')
      expect(experiment(:stub).assigned.name).to eq('foo')
    end
  end
end

RSpec.describe Gitlab::Experiment::RSpecMatchers, :experiment do
  subject_experiment_class = Class.new(Gitlab::Experiment) do
    def candidate_behavior
      '_candidate_'
    end
  end

  subject(:subject_experiment) { subject_experiment_class.new(:example) }

  after do
    subject_experiment.class.reset_callbacks(:exclusion_check)
    subject_experiment.class.reset_callbacks(:segmentation)
    subject_experiment.class.reset_callbacks(:segmentation_skipped)
  end

  it "injects itself into rspec correctly" do
    all_blocks = RSpec.configuration.instance_variable_get(:@derived_metadata_blocks)
    applicable_blocks = all_blocks.items_for(file_path: '/spec/experiments/my_experiment_spec.rb')
    expect(applicable_blocks.length).to be >= 1

    applicable_blocks[0].call(metadata = { foo: :bar })

    expect(metadata).to include(type: :experiment)
  end

  describe "the `exclude` matcher" do
    let(:matcher) { exclude(foo: :bar) }

    before do
      subject_experiment.class.exclude { context.value[:foo] == :bar }
    end

    it "is generally supported" do
      expect(subject_experiment).to exclude(foo: :bar)
      expect(subject_experiment).not_to exclude(foo: :baz)
    end

    it "raises an exception if you try to use it on non experiments" do
      expect { matcher.matches?(1) }.to raise_error(
        ArgumentError,
        'exclude matcher is limited to experiment instances'
      )
    end

    it "raises an exception if you try to use it on experiment classes" do
      expect { matcher.matches?(Gitlab::Experiment) }.to raise_error(
        ArgumentError,
        'exclude matcher requires an instance of an experiment'
      )
    end

    context "with failure messages" do
      before do
        matcher.matches?(subject_experiment) # this is never how you'd use it, it's only for testing messages
      end

      it "generates a nice failure message" do
        expect(matcher.failure_message).to eq <<~MESSAGE.strip
          expected {:foo=>:bar} to be excluded
        MESSAGE
      end

      context "when negated" do
        it "generates a negated failure message" do
          expect(matcher.failure_message_when_negated).to eq <<~MESSAGE.strip
            expected {:foo=>:bar} not to be excluded
          MESSAGE
        end
      end
    end
  end

  describe "the `segment` matcher" do
    let(:matcher) { segment(foo: :quz) }

    before do
      subject_experiment.class.segment(variant: :candidate) { context.value[:foo] == :bar }
      subject_experiment.class.segment(variant: :control) { context.value[:foo] == :baz }
    end

    it "is generally supported" do
      expect(subject_experiment).to segment(foo: :bar)
      expect(subject_experiment).not_to segment(foo: :quz)
    end

    it "supports chaining `into` to specify the variant" do
      expect(subject_experiment).to segment(foo: :bar).into(:candidate)
      expect(subject_experiment).to segment(foo: :baz).into(:control)
      expect(subject_experiment).not_to segment(foo: :baz).into(:candidate)
    end

    it "raises an exception if you try to use it on non experiments" do
      expect { matcher.matches?(1) }.to raise_error(
        ArgumentError,
        'segment matcher is limited to experiment instances'
      )
    end

    it "raises an exception if you try to use it on experiment classes" do
      expect { matcher.matches?(Gitlab::Experiment) }.to raise_error(
        ArgumentError,
        'segment matcher requires an instance of an experiment'
      )
    end

    context "with failure messages" do
      before do
        matcher.matches?(subject) # this is never how you'd use it, it's only for testing messages
      end

      it "generates a nice failure message" do
        expect(matcher.failure_message).to eq <<~MESSAGE.strip
          expected {:foo=>:quz} to be segmented
        MESSAGE
      end

      it "includes the variant details if present" do
        matcher.instance_variable_set(:@expected, :expected_variant)
        matcher.instance_variable_set(:@actual, :actual_variant)
        expect(matcher.failure_message).to eq <<~MESSAGE.strip
          expected {:foo=>:quz} to be segmented into variant
              expected variant: expected_variant
                actual variant: actual_variant
        MESSAGE
      end

      context "when negated" do
        it "generates a nice failure message" do
          expect(matcher.failure_message_when_negated).to eq <<~MESSAGE.strip
            expected {:foo=>:quz} not to be segmented
          MESSAGE
        end

        it "includes the variant details if present" do
          matcher.instance_variable_set(:@expected, :expected_variant)
          matcher.instance_variable_set(:@actual, :actual_variant)
          expect(matcher.failure_message_when_negated).to eq <<~MESSAGE.strip
            expected {:foo=>:quz} not to be segmented into variant
                expected variant: expected_variant
                  actual variant: actual_variant
          MESSAGE
        end
      end
    end
  end

  describe "the `track` matcher" do
    let(:matcher) { track(:foo) }

    it "is generally supported" do
      expect(subject_experiment).to track(:foo)

      subject_experiment.track(:foo)
      subject_experiment.track(:bar)
    end

    it "calls the original track method" do
      called_with_args = nil
      subject.define_singleton_method(:track) { |*args| called_with_args = args }

      expect(subject).to track(:event_name)
      subject.track(:event_name)

      expect(called_with_args).to eq([:event_name])
    end

    it "supports matching on negation" do
      expect(subject_experiment).not_to track(:bar)

      subject_experiment_class.new(:example).track(:foo)
    end

    it "doesn't call the original track method on negation" do
      subject.define_singleton_method(:track) { |*| }

      expect(subject).to receive(:track).once # this should only happen once

      expect(subject).not_to track(:event_name)
      subject.track(:foo)
    end

    it "handles passing the class" do
      expect(subject_experiment_class).to track(:foo)

      subject_experiment.track(:foo)
    end

    it "supports chaining `for` to specify the variant" do
      subject_experiment.assigned(:candidate)

      expect(subject_experiment).to track(:foo).for(:candidate)

      subject_experiment.track(:foo)
    end

    it "supports chaining `with_context` to specify the context" do
      subject_experiment.context(foo: :bar)

      expect(subject_experiment).to track(:foo).with_context(foo: :bar)

      subject_experiment.track(:foo)
    end

    it "supports chaining `on_next_instance` to adjust how expectations are applied" do
      # Passing an instance -- only the passed experiment instance will be stubbed.
      expect(subject_experiment).to track(:foo)
      subject_experiment.track(:foo)

      # Passing an instance and asking to stub all instances -- all future instances of the experiment will be stubbed.
      expect(subject_experiment).to track(:bar).on_next_instance
      subject_experiment_class.new(:example).track(:bar)

      # Passing a class -- all future instances of the experiment will be stubbed.
      expect(subject_experiment.class).to track(:baz)
      subject_experiment_class.new(:example).track(:baz)
    end

    it "raises an exception if you try to use it on non experiments" do
      expect { matcher.matches?(1) }.to raise_error(
        ArgumentError,
        'track matcher is limited to experiment instances and classes'
      )
    end

    it "allows stubbing experiments and mocking tracking" do
      # This is notable because there's not a way in rspec currently to accomplish the any_instance_of behaviors.
      # Wrapping :new multiple times isn't possible, so this test covers that we can stub and use the track  matcher
      # together.
      #
      # Here's an example of what's not possible:
      #
      # allow(StubExperiment).to receive(:new).and_wrap_original do |new, *args, &block|
      #   puts "first wrapping"
      #   new.call(*args, &block)
      # end
      #
      # allow(StubExperiment).to receive(:new).and_wrap_original do |new, *args, &block|
      #   puts "second wrapping"
      #   new.call(*args, &block)
      # end
      #
      # > StubExperiment.new
      # "second wrapping"
      stub_experiments(stub: :bar)

      stub = experiment(:stub)

      expect(stub.assigned.name).to eq('bar')
      expect(stub).to track(:foo).for(:bar).on_next_instance

      new_stub = experiment(:stub)
      new_stub.track(:foo)
      expect(new_stub.assigned.name).to eq('bar')
    end

    context "with failure messages" do
      it "should be tested, but it's actually pretty involved"
    end
  end
end
