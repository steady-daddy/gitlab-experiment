# frozen_string_literal: true

module StubRailsExampleGroup
  def controller(*_); end

  def tests(*_); end

  def arguments(*_); end

  def destination(*_); end
end

if defined?(Rails)
  module GeneratorSpec::BlockOverrides
    def tests(klass = nil)
      test_case.generator_class = block_given? ? yield : klass
    end

    def arguments(array = nil)
      test_case.default_arguments = block_given? ? yield : array
    end

    def destination(path = nil)
      test_case.destination_root = block_given? ? yield : path
    end

    def described_class
      test_case.generator_class
    end
  end

  RSpec.configure do |config|
    config.filter_rails_from_backtrace!
    config.extend GeneratorSpec::BlockOverrides, type: :generator
    config.around(:example, type: :generator) do |example|
      prepare_destination
      example.run
    ensure
      prepare_destination
    end
  end
else
  RSpec.configure do |config|
    config.extend StubRailsExampleGroup, type: :controller
    config.extend StubRailsExampleGroup, type: :generator
    config.before(:example, rails: true) { skip "not running without rails" }
  end
end
