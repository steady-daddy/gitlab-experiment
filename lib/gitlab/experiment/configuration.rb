# frozen_string_literal: true

require 'singleton'
require 'logger'
require 'digest'

require 'active_support/deprecation'

module Gitlab
  class Experiment
    class Configuration
      include Singleton

      # Prefix all experiment names with a given string value.
      # Use `nil` for no prefix.
      @name_prefix = nil

      # The logger can be used to log various details of the experiments.
      @logger = Logger.new($stdout)

      # The base class that should be instantiated for basic experiments.
      # It should be a string, so we can constantize it later.
      @base_class = 'Gitlab::Experiment'

      # The caching layer is expected to match the Rails.cache interface.
      # If no cache is provided some rollout strategies may behave differently.
      # Use `nil` for no caching.
      @cache = nil

      # The domain to use on cookies.
      #
      # When not set, it uses the current host. If you want to provide specific
      # hosts, you use `:all`, or provide an array.
      #
      # Examples:
      #   nil, :all, or ['www.gitlab.com', '.gitlab.com']
      @cookie_domain = :all

      # The default rollout strategy.
      #
      # The recommended default rollout strategy when not using caching would
      # be `Gitlab::Experiment::Rollout::Percent` as that will consistently
      # assign the same variant with or without caching. The base strategy is
      # minimal and doesn't handle multiple variants but is useful when writing
      # your own rollout strategies.
      #
      # Each experiment can specify its own rollout strategy:
      #
      # class ExampleExperiment < ApplicationExperiment
      #   default_rollout :percent # or Gitlab::Experiment::Rollout::Percent,
      #                            # or :random, :round_robin, etc.
      # end
      #
      # Included rollout strategies:
      #   Gitlab::Experiment::Rollout::Base, (minimal single variant strategy)
      #   Gitlab::Experiment::Rollout::Percent, (recommended)
      #   Gitlab::Experiment::Rollout::RoundRobin, or
      #   Gitlab::Experiment::Rollout::Random
      @default_rollout = Rollout::Base.new

      # Secret seed used in generating context keys.
      #
      # You'll typically want to use an environment variable or secret value
      # for this.
      #
      # Consider not using one that's shared with other systems, like Rails'
      # SECRET_KEY_BASE for instance. Generate a new secret and utilize that
      # instead.
      @context_key_secret = nil

      # Bit length used by SHA2 in generating context keys.
      #
      # Using a higher bit length would require more computation time.
      #
      # Valid bit lengths:
      #   256, 384, or 512
      @context_key_bit_length = 256

      # The default base path that the middleware (or rails engine) will be
      # mounted. The middleware enables an instrumentation url, that's similar
      # to links that can be instrumented in email campaigns.
      #
      # Use `nil` if you don't want to mount the middleware.
      #
      # Examples:
      #   '/-/experiment', '/redirect', nil
      @mount_at = nil

      # When using the middleware, links can be instrumented and redirected
      # elsewhere. This can be exploited to make a harmful url look innocuous
      # or that it's a valid url on your domain. To avoid this, you can provide
      # your own logic for what urls will be considered valid and redirected
      # to.
      #
      # Expected to return a boolean value.
      @redirect_url_validator = ->(_redirect_url) { true }

      # Logic this project uses to determine inclusion in a given experiment.
      #
      # Expected to return a boolean value.
      #
      # This block is executed within the scope of the experiment and so can
      # access experiment methods, like `name`, `context`, and `signature`.
      @inclusion_resolver = ->(_requested_variant) { false }

      # Tracking behavior can be implemented to link an event to an experiment.
      #
      # This block is executed within the scope of the experiment and so can
      # access experiment methods, like `name`, `context`, and `signature`.
      @tracking_behavior = lambda do |event, args|
        # An example of using a generic logger to track events:
        Configuration.logger.info("#{self.class.name}[#{name}] #{event}: #{args.merge(signature: signature)}")

        # Using something like snowplow to track events (in gitlab):
        #
        # Gitlab::Tracking.event(name, event, **args.merge(
        #   context: (args[:context] || []) << SnowplowTracker::SelfDescribingJson.new(
        #     'iglu:com.gitlab/gitlab_experiment/jsonschema/0-2-0', signature
        #   )
        # ))
      end

      # Called at the end of every experiment run, with the result.
      #
      # You may want to track that you've assigned a variant to a given
      # context, or push the experiment into the client or publish results
      # elsewhere like into redis.
      #
      # This block is executed within the scope of the experiment and so can
      # access experiment methods, like `name`, `context`, and `signature`.
      @publishing_behavior = lambda do |_result|
        # Track the event using our own configured tracking logic.
        track(:assignment)

        # Push the experiment knowledge into the front end. The signature
        # contains the context key, and the variant that has been determined.
        #
        # Gon.push({ experiment: { name => signature } }, true)

        # Log using our logging system, so the result (which can be large) can
        # be reviewed later if we want to.
        #
        # Lograge::Event.log(experiment: name, result: result, signature: signature)
      end

      class << self
        # TODO: Added deprecation in release 0.6.0
        def context_hash_strategy=(block)
          ActiveSupport::Deprecation.warn('context_hash_strategy has been deprecated, instead configure' \
            ' `context_key_secret` and `context_key_bit_length`.')
          @__context_hash_strategy = block
        end

        # TODO: Added deprecation in release 0.5.0
        def variant_resolver
          ActiveSupport::Deprecation.warn('variant_resolver is deprecated, instead use `inclusion_resolver` with a' \
            ' block that returns a boolean.')
          @inclusion_resolver
        end

        def variant_resolver=(block)
          ActiveSupport::Deprecation.warn('variant_resolver is deprecated, instead use `inclusion_resolver` with a' \
            ' block that returns a boolean.')
          @inclusion_resolver = block
        end

        attr_accessor(
          :name_prefix,
          :logger,
          :base_class,
          :cache,
          :cookie_domain,
          :context_key_secret,
          :context_key_bit_length,
          :mount_at,
          :default_rollout,
          :redirect_url_validator,
          :inclusion_resolver,
          :tracking_behavior,
          :publishing_behavior
        )
      end
    end
  end
end
