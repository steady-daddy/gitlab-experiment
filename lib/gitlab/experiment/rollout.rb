# frozen_string_literal: true

module Gitlab
  class Experiment
    module Rollout
      autoload :Percent, 'gitlab/experiment/rollout/percent.rb'
      autoload :Random, 'gitlab/experiment/rollout/random.rb'
      autoload :RoundRobin, 'gitlab/experiment/rollout/round_robin.rb'

      def self.resolve(klass)
        return "#{name}::#{klass.to_s.classify}".constantize if klass.is_a?(Symbol) || klass.is_a?(String)

        klass
      end

      class Base
        attr_reader :experiment

        delegate :variant_names, :cache, :id, to: :experiment

        def initialize(options = {})
          @options = options
          # validate! # we want to validate here, but we can't yet
        end

        def rollout_for(experiment)
          @experiment = experiment
          validate! # until we have variant registration we can only validate here
          execute
        end

        def validate!
          # base is always valid
        end

        def execute
          variant_names.first
        end
      end
    end
  end
end
