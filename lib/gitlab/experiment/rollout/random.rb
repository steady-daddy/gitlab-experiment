# frozen_string_literal: true

# The random rollout strategy will randomly assign a variant when the context is determined to be within the experiment
# group.
#
# If caching is enabled this is a predicable and consistent assignment, but if caching isn't enabled, assignment will be
# random each time.
#
# class ExampleExperiment < ApplicationExperiment
#   default_rollout :random # randomized between red and blue (mostly even distribution)
#
#   def control_behavior; end
#   def red_behavior; end
#   def blue_behavior; end
# end
#
module Gitlab
  class Experiment
    module Rollout
      class Random < Base
        def execute
          variant_names.sample # pick a random variant
        end
      end
    end
  end
end
