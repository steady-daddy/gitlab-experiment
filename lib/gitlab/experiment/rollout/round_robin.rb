# frozen_string_literal: true

# The round robin strategy will assign the next variant in the list, looping back to the first variant after all
# variants have been assigned. This is useful for very small sample sizes where very even distribution can be required.
#
# Requires a cache to be configured.
#
# Keeps track of the number of assignments into the experiment group, and uses this to rotate "round robin" style
# through the variants that are defined.
#
# class ExampleExperiment < ApplicationExperiment
#   default_rollout :round_robin # rotate through red and blue
#
#   def control_behavior; end
#   def red_behavior; end
#   def blue_behavior; end
# end
#
module Gitlab
  class Experiment
    module Rollout
      class RoundRobin < Base
        KEY_NAME = :last_round_robin_variant

        def execute
          variant_names[(cache.attr_inc(KEY_NAME) - 1) % variant_names.size]
        end
      end
    end
  end
end
