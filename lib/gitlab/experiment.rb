# frozen_string_literal: true

require 'scientist'
require 'request_store'
require 'active_support'
require 'active_support/callbacks'
require 'active_support/cache'
require 'active_support/concern'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/string/inflections'
require 'active_support/core_ext/module/delegation'

require 'gitlab/experiment/errors'
require 'gitlab/experiment/base_interface'
require 'gitlab/experiment/cache'
require 'gitlab/experiment/callbacks'
require 'gitlab/experiment/rollout'
require 'gitlab/experiment/configuration'
require 'gitlab/experiment/cookies'
require 'gitlab/experiment/context'
require 'gitlab/experiment/dsl'
require 'gitlab/experiment/middleware'
require 'gitlab/experiment/nestable'
require 'gitlab/experiment/variant'
require 'gitlab/experiment/version'
require 'gitlab/experiment/engine' if defined?(Rails::Engine)

module Gitlab
  class Experiment
    include BaseInterface
    include Cache
    include Callbacks
    include Nestable

    class << self
      # Class level callback registration methods.

      def exclude(*filter_list, **options, &block)
        build_exclude_callback(filter_list.unshift(block), **options)
      end

      def segment(*filter_list, variant:, **options, &block)
        build_segment_callback(filter_list.unshift(block), variant, **options)
      end

      def before_run(*filter_list, **options, &block)
        build_run_callback(filter_list.unshift(:before, block), **options)
      end

      def around_run(*filter_list, **options, &block)
        build_run_callback(filter_list.unshift(:around, block), **options)
      end

      def after_run(*filter_list, **options, &block)
        build_run_callback(filter_list.unshift(:after, block), **options)
      end

      # Class level definition methods.

      def default_rollout(rollout = nil, options = {})
        return @_rollout ||= Configuration.default_rollout if rollout.blank?

        @_rollout = Rollout.resolve(rollout).new(options)
      end

      # Class level accessor methods.

      def published_experiments
        RequestStore.store[:published_gitlab_experiments] || {}
      end
    end

    def name
      [Configuration.name_prefix, @_name].compact.join('_')
    end

    def control(&block)
      variant(:control, &block)
    end

    def candidate(name = nil, &block)
      # TODO: Added deprecation in release 0.6.5
      if name.present?
        ActiveSupport::Deprecation.warn('passing a name to the `candidate` method is deprecated, instead use `variant`')
      end

      variant(name || :candidate, &block)
    end

    def variant(name = nil, &block)
      if block.present? # we know we're defining a variant block
        raise ArgumentError, 'missing variant name' if name.blank?

        return behaviors[name.to_s] = block
      end

      # TODO: Added deprecation in release 0.6.5
      if name.present?
        ActiveSupport::Deprecation.warn(
          "assigning the variant using `variant` is deprecated, instead use `assigned(#{name})`"
        )
      else
        ActiveSupport::Deprecation.warn(
          'getting the assigned variant using `variant` is deprecated, instead use `assigned`'
        )
      end

      assigned(name)
    end

    def context(value = nil)
      return @_context if value.blank?

      @_context.value(value)
      @_context
    end

    def assigned(value = nil)
      @_assigned_variant_name = cache_variant(value) if value.present?
      if @_assigned_variant_name || @_resolving_variant
        return Variant.new(name: (@_assigned_variant_name || :unresolved).to_s)
      end

      if enabled?
        @_resolving_variant = true
        @_assigned_variant_name = cached_variant_resolver(@_assigned_variant_name)
      end

      run_callbacks(segmentation_callback_chain) do
        @_assigned_variant_name ||= :control
        Variant.new(name: @_assigned_variant_name.to_s)
      end
    ensure
      @_resolving_variant = false
    end

    def rollout(rollout = nil, options = {})
      return @_rollout ||= self.class.default_rollout(nil, options) if rollout.blank?

      @_rollout = Rollout.resolve(rollout).new(options)
    end

    def exclude!
      @_excluded = true
    end

    def run(variant_name = nil)
      return @_result if context.frozen?

      @_result = run_callbacks(run_callback_chain) { super(assigned(variant_name).name) }
    rescue Scientist::BehaviorMissing => e
      raise Error, e
    end

    def publish(result)
      instance_exec(result, &Configuration.publishing_behavior)

      (RequestStore.store[:published_gitlab_experiments] ||= {})[name] = signature.merge(excluded: excluded?)
    end

    def track(action, **event_args)
      return unless should_track?

      instance_exec(action, event_args, &Configuration.tracking_behavior)
    end

    def process_redirect_url(url)
      return unless Configuration.redirect_url_validator&.call(url)

      track('visited', url: url)
      url # return the url, which allows for mutation
    end

    def enabled?
      true
    end

    def excluded?
      return @_excluded if defined?(@_excluded)

      @_excluded = !run_callbacks(exclusion_callback_chain) { :not_excluded }
    end

    def experiment_group?
      instance_exec(@_assigned_variant_name, &Configuration.inclusion_resolver)
    end

    def should_track?
      enabled? && context.trackable? && !excluded?
    end

    def signature
      { variant: assigned.name, experiment: name }.merge(context.signature)
    end

    def key_for(source, seed = name)
      # TODO: Added deprecation in release 0.6.0
      if (block = Configuration.instance_variable_get(:@__context_hash_strategy))
        return instance_exec(source, seed, &block)
      end

      return source if source.is_a?(String)

      source = source.keys + source.values if source.is_a?(Hash)

      ingredients = Array(source).map { |v| identify(v) }
      ingredients.unshift(seed).unshift(Configuration.context_key_secret)

      Digest::SHA2.new(Configuration.context_key_bit_length).hexdigest(ingredients.join('|'))
    end

    protected

    def identify(object)
      (object.respond_to?(:to_global_id) ? object.to_global_id : object).to_s
    end

    def resolve_variant_name
      rollout.rollout_for(self) if experiment_group?
    end
  end
end
