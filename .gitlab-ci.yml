stages:
- test
- verify
- release

default:
  image: ruby:$RUBY_VERSION
  tags:
  - gitlab-org
  - docker

variables:
  RUBY_VERSION: '2.7'
  REDIS_HOST: redis
  FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR: 'true'

services:
- redis:latest

include:
# Code quality customization: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#example-configuration
- template: Code-Quality.gitlab-ci.yml
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
- template: Jobs/SAST.gitlab-ci.yml
- project: gitlab-org/quality/pipeline-common
  file: /ci/danger-review.yml

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == "master" # for `master` branch (pushes. merges etc.)
    - if: "$CI_MERGE_REQUEST_IID" # for all merge requests
    - if: "$CI_COMMIT_TAG" # for all tags

brakeman-sast:
  extends: .sast-analyzer
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /brakeman/
      when: never
    - if: $CI
      exists:
      - '**/*.rb'
      - '**/Gemfile'

rubocop:
  stage: test
  before_script:
  - bundle install -j $(nproc) --path vendor/cache
  script:
  - bundle exec rubocop --parallel
  cache:
    key: '1'
    paths:
    - vendor/cache

spec:default:
  stage: test
  before_script:
  - bundle install -j $(nproc) --path vendor/cache
  script:
  - bundle exec rake
  cache:
    key: '1'
    paths:
    - vendor/cache
  artifacts:
    reports:
      cobertura: coverage/coverage.xml
  parallel:
    matrix:
    - RUBY_VERSION: '2.7'
    - RUBY_VERSION: '3.0'

spec:without_rails:
  stage: test
  before_script:
  - bundle install -j $(nproc) --path vendor/cache
  script:
  - bundle exec rake spec:without_rails
  cache:
    key: '1'
    paths:
    - vendor/cache

code_quality:
  stage: verify # longest running job, save on compute and run after test
  needs: # override no dependency setting in template, this reverts it to depend on stage test
  rules:
  - if: "$CI_MERGE_REQUEST_IID" # for all merge requests
  - if: $CI_COMMIT_BRANCH == "master" # for merge request widget diff base reports

release_stage:
  stage: release
  rules:
  - if: "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^release-/" # for release branches
  before_script:
  - bundle install -j $(nproc) --path vendor/cache
  script:
  - bundle exec bin/release stage --force

release_finalize:
  stage: release
  rules:
  - if: "$CI_COMMIT_TAG =~ /^v\\d/" # for version tags
  before_script:
  - bundle install -j $(nproc) --path vendor/cache
  script:
  - bundle exec bin/release finalize --force
  artifacts:
    paths:
    - gitlab-experiment*.gem
